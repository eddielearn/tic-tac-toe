function Gameboard(players) {
    const rows = 3;
    const columns = 3;
    let board = initializeBoard();

    function initializeBoard() {
        const initialBoard = [];
        for (let i = 0; i < rows; i++) {
            initialBoard[i] = [];
            for (let j = 0; j < columns; j++) {
                initialBoard[i].push(Cell());
            }
        }
        return initialBoard;
    }

    const getBoard = () => board;

    const placeToken = (row, column, player) => {
        if (board[row][column].getValue() === null) {
            board[row][column].addToken(player);
            return true;
        }
        return false;
    };

    const checkWin = () => {
        // Check rows
        for (let i = 0; i < rows; i++) {
            if (
                board[i][0].getValue() !== null &&
                board[i][0].getValue() === board[i][1].getValue() &&
                board[i][0].getValue() === board[i][2].getValue()
            ) {
            return players.find(player => player.token === board[i][0].getValue());
            }
        }

        // Check columns
        for (let j = 0; j < columns; j++) {
            if (
                board[0][j].getValue() !== null &&
                board[0][j].getValue() === board[1][j].getValue() &&
                board[0][j].getValue() === board[2][j].getValue()
            ) {
                return players.find(player => player.token === board[0][j].getValue());
            }
        }

        // Check diagonals
        if (
            board[0][0].getValue() !== null &&
            board[0][0].getValue() === board[1][1].getValue() &&
            board[0][0].getValue() === board[2][2].getValue()
        ) {
            return players.find(player => player.token === board[0][0].getValue());
        }

        if (
            board[0][2].getValue() !== null &&
            board[0][2].getValue() === board[1][1].getValue() &&
            board[0][2].getValue() === board[2][0].getValue()
        ) {
            return players.find(player => player.token === board[0][2].getValue());
        }

        // No winner
        return null;
    };

    const resetBoard = () => {
        board = initializeBoard();        
    };

    return { getBoard, placeToken, checkWin, resetBoard };
}

function Cell() {
    let value = null;

    const addToken = (player) => {
        value = player;
    };

    const getValue = () => value;

    return { addToken, getValue };
}

function GameController(playerOneName = 'Player One', playerTwoName = 'Player Two') {

    const players = [
        { name: playerOneName, token: 'X' },
        { name: playerTwoName, token: 'O' }
    ];

    const board = Gameboard(players);

    let activePlayer = players[0];

    const switchPlayerTurn = () => {
        activePlayer = activePlayer === players[0] ? players[1] : players[0];
    };

    const getActivePlayer = () => activePlayer;

    const playRound = (row, column) => {
        if (board.placeToken(row, column, getActivePlayer().token)) {
            const winner = board.checkWin();
            if (winner) {
                console.log(`${winner.name} wins!!!`);
                return winner;
            }
            switchPlayerTurn();
        } else {
            console.log('Cell is already occupied!');
        }
        return null; // Return null if there's no winner yet
    };

    // Method to reset game
    const resetGame = () => {
        board.resetBoard(); // Reset the board through Gameboard's resetBoard method
        activePlayer = players[0]; // Reset active player to the first player
    };

    return {
        playRound,
        getActivePlayer,
        getBoard: board.getBoard,  // Directly return the method reference
        checkWin: board.checkWin,   // Ensure checkWin is accessible from GameController
        resetGame
    };
}

function ScreenController() {
    const game = GameController();
    const playerTurnDiv = document.querySelector('.turn');
    const boardDiv = document.querySelector('.board');
    const winnerDiv = document.querySelector('.winner');
    const resetButton = document.querySelector('.reset-button');

    const updateScreen = () => {
        // Clear the board
        boardDiv.textContent = '';

        // get the newest version of the board and player turn
        const board = game.getBoard();
        const activePlayer = game.getActivePlayer();

        // Display player's turn
        playerTurnDiv.textContent = `${activePlayer.name}'s turn...`;

        // Render board squares
        board.forEach((row, rowIndex) => {
            row.forEach((cell, colIndex) => {
                // Anything clickable should be a button
                const cellButton = document.createElement('button');
                cellButton.classList.add('cell');

                // Create a data attribute to identify the row and column
                cellButton.dataset.row = rowIndex;
                cellButton.dataset.column = colIndex;
                cellButton.textContent = cell.getValue() || '';
                cellButton.addEventListener('click', () => handleCellClick(rowIndex, colIndex));
                boardDiv.appendChild(cellButton);
            });
        });

        const winner = game.checkWin();
        if (winner) {
            winnerDiv.textContent = `${winner.name} wins!!!`;
            // Optionally disable further moves or add other end-of-game actions
        } else {
            winnerDiv.textContent = '';
        }
    };

    // Click handler for cell buttons
    function handleCellClick(row, column) {
        game.playRound(row, column);
        updateScreen();
    }

    resetButton.addEventListener('click', () => {
        // Reset game state
        game.resetGame();
        updateScreen();
    });

    // Initial render
    updateScreen();
}

ScreenController();